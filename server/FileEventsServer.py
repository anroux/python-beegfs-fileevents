#!/usr/bin/env python3
import os
import sys
import socket

import threading
import multiprocessing

from fileevent import Reader, FileEvent

class FileEventsServer(threading.Thread):
    def __init__(self, socket_path, on_fileevent):
        super().__init__()
        self.running = False
        self.clients = []

        self.on_fileevent = on_fileevent

        try:
            os.unlink(socket_path)
        except OSError:
            if os.path.exists(socket_path):
                raise

        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_SEQPACKET)

        self.sock.bind(socket_path)
        os.chmod(socket_path, 0o666)

    def run(self):
        self.running = True

        self.sock.listen(5)
        while self.running:
            try:
                connection, client_address = self.sock.accept()
                client = FileEventsClient(connection, self)
                self.clients.append(client)

                client.start()
            except Exception as ex:
                if self.running:
                    print(ex)

    def stop(self):
        self.running = False

        for client in self.clients:
            client.stop()
            client.terminate()
            client.join()

        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()


class FileEventsClient(multiprocessing.Process):
    def __init__(self, connection, parent):
        super().__init__()
        self.connection = connection
        self.parent = parent

    def read_packet(self):
        buf = self.connection.recv(65536)
        reader = Reader(buf)

        return reader.read_packet()

    def run(self):
        try:
            while True:
                event = self.read_packet()
                self.parent.on_fileevent(event)
        finally:
            self.connection.close()

    def stop(self):
        self.connection.shutdown(socket.SHUT_RDWR)
        self.connection.close()
