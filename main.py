#!/usr/bin/env python3
import os
import time
import signal
import pika

from server import FileEventsServer
from fileevent import FileEvent, FileEventEncoder, EventType

def file_event_path():
    with open("/etc/beegfs/beegfs-meta.conf") as f:
        for line in f:
            line = line.strip()

            if len(line) == 0 or line.startswith("#"):
                continue

            key, value = map(lambda x:x.strip(), line.split("="))

            if key == "sysFileEventLogTarget":
                protocol, path = value.split(":")
                if protocol != "unix":
                    raise Exception("{} is not a UNIX socket path".format(value))

                return path

    raise Exception("Unable to locate eventlog socket path")

class OnFileEvent(object):
    RECONNECT_RETRIES = 3

    def __init__(self):
        self.encoder = FileEventEncoder()

        self.connection = None
        self.channel = None

        self.reconnect()

    def reconnect(self):
        try:
            if self.connection is not None and self.connection.is_open:
                self.connection.close()
        except:
            pass

        self.connection = pika.BlockingConnection(pika.ConnectionParameters('gordrakk.esrf.fr'))
        self.channel = self.connection.channel()

        self.channel.queue_declare(queue='commits')
        self.channel.exchange_declare(exchange='commits', exchange_type='direct')
        self.channel.queue_bind(exchange='commits', queue='commits')

        self.channel.exchange_declare(exchange='events', exchange_type='fanout')

    def emit(self, event, queue, retries=0):
        if retries > self.RECONNECT_RETRIES:
            raise Exception("Can't emit message. Too many tries")

        if self.connection is None:
            self.reconnect()

        try:
            self.channel.basic_publish(exchange=queue,
                routing_key=queue,
                body=self.encoder.encode(event))
        except:
            if retries > 0:
                time.sleep(.2)

            self.reconnect()
            self.emit(event, queue, retries=retries + 1)

    def __call__(self, event):
        self.emit(event, 'events')

        if event._type == EventType.CLOSE_WRITE:
            self.emit(event, 'commits')


if __name__ == "__main__":
    on_fileevent = OnFileEvent()

    fe_server = FileEventsServer(file_event_path(), on_fileevent)
    fe_server.start()

    try:
        signal.pause()
    except KeyboardInterrupt:
        fe_server.stop()
        fe_server.join()
