#!/usr/bin/env python3
from struct import unpack, calcsize
from . import FileEvent, EventType


class Reader(object):
    HEADER_FMT = "<HHI"
    HEADER_SIZE = calcsize(HEADER_FMT)

    STAT_FMT = "<QQ"
    STAT_SIZE = calcsize(STAT_FMT)

    def __init__(self, buf):
        self.buf = buf
        self.position = 0

    def read_uint32(self):
        (number,) = unpack("<I", self.buf[self.position:self.position + 4])
        self.position += 4

        return number

    def read_header(self):
        major, minor, size = unpack(self.HEADER_FMT, self.buf[self.position:self.position + self.HEADER_SIZE])
        self.position += self.HEADER_SIZE

        return major, minor, size

    def read_stats(self):
        dropped_seq, missed_seq = unpack(self.STAT_FMT, self.buf[self.position:self.position + self.STAT_SIZE])
        self.position += self.STAT_SIZE

        return dropped_seq, missed_seq

    def read_string(self):
        size = self.read_uint32()

        (content,) = unpack("<" + str(size) + "s", self.buf[self.position:self.position + size])
        self.position += size + 1

        return content.decode('utf-8')

    def read_packet(self):
        file_event = FileEvent()

        file_event.major, file_event.minor, size = self.read_header()
        if file_event.major != 1 or file_event.minor != 0:
            raise Exception("Non supported format")

        if len(self.buf) != size:
            raise Exception("Invalid size")

        file_event.dropped_seq, file_event.missed_seq = self.read_stats()

        file_event._type = EventType(self.read_uint32())

        file_event.entry_id = self.read_string()
        file_event.parent_entry_id = self.read_string()
        file_event.path = self.read_string()
        file_event.target_path = self.read_string()
        file_event.target_parent_id = self.read_string()

        return file_event
