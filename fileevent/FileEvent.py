#!/usr/bin/env python3
import json

from enum import IntEnum


class EventType(IntEnum):
    FLUSH       =  0
    TRUNCATE    =  1
    SETATTR     =  2
    CLOSE_WRITE =  3
    CREATE      =  4
    MKDIR       =  5
    MKNOD       =  6
    SYMLINK     =  7
    RMDIR       =  8
    UNLINK      =  9
    HARDLINK    = 10
    RENAME      = 11


class FileEvent(object):
    def __init___(self):
        self.dropped_seq = None
        self.missed_seq = None

        self._type = None

        self.entry_id = None
        self.parent_entry_id = None
        self.path = None
        self.target_path = None
        self.target_parent_id = None

    def __repr__(self):
        obj_repr = "{:<15} [{:>15} > {:<15}] {}".format(
            self._type.name,
            self.parent_entry_id,
            self.entry_id, self.path)
        if len(self.target_parent_id) > 0:
            obj_repr += " -> {} ({})".format(self.target_path, self.target_parent_id)

        return obj_repr


class FileEventEncoder(json.JSONEncoder):
    def default(self, fe):
        if isinstance(fe, FileEvent):
            out = {
                "dropped_seq":     fe.dropped_seq,
                "missed_seq":      fe.missed_seq,
                "type":            fe._type.name,
                "entry_id":        fe.entry_id,
                "parent_entry_id": fe.parent_entry_id,
                "path":            fe.path,
            }

            if len(fe.target_parent_id) > 0:
                out.update({
                    "target_path":      fe.target_path,
                    "target_parent_id": fe.target_parent_id
                })

            return out
        else:
            return super().default(fe)

